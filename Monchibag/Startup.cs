﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Monchibag
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile($"securitysettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"securitysettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region ///CONTEXT
            var migrationsAssembly = typeof(Startup).Assembly.GetName().Name;
            services.AddDbContext<DDDShell.Context.DDDShellDbContext>(o =>
                o.UseSqlServer(
                    Configuration.GetConnectionString("DDDShellSecurity_ConnectionString"),
                    opt => opt.MigrationsAssembly(migrationsAssembly)));
            #endregion

            #region /// DEPENDENCY INJECTION
            services.AddScoped<DDDShell.Domain.AggregateModels.ICipherService, DDDShell.Infrastructure.Services.CipherService>();
            services.AddScoped<DDDShell.Domain.AggregateModels.IEmailService, DDDShell.Infrastructure.Services.MailKitEmailService>();
            #endregion

            #region /// IDENTITY
            services.AddIdentity<DDDShell.Security.Identity.User, DDDShell.Security.Identity.Role>(identityOptions =>
            {
                identityOptions.Password.RequireDigit = Configuration.GetValue<bool>("security:identityOptions:Password:RequireDigit");
                identityOptions.Password.RequiredLength = Configuration.GetValue<int>("security:identityOptions:Password:RequiredLength");
                identityOptions.Password.RequiredUniqueChars = Configuration.GetValue<int>("security:identityOptions:Password:RequiredUniqueChars");
                identityOptions.Password.RequireLowercase = Configuration.GetValue<bool>("security:identityOptions:Password:RequireLowercase");
                identityOptions.Password.RequireNonAlphanumeric = Configuration.GetValue<bool>("security:identityOptions:Password:RequireNonAlphanumeric");
                identityOptions.Password.RequireUppercase = Configuration.GetValue<bool>("security:identityOptions:Password:RequireUppercase");
                identityOptions.User.RequireUniqueEmail = Configuration.GetValue<bool>("security:identityOptions:User:RequireUniqueEmail");
                identityOptions.SignIn.RequireConfirmedEmail = Configuration.GetValue<bool>("security:identityOptions:SignIn:RequireConfirmedEmail");
                identityOptions.SignIn.RequireConfirmedPhoneNumber = Configuration.GetValue<bool>("security:identityOptions:SignIn:RequireConfirmedPhoneNumber");
            })
                .AddUserManager<DDDShell.Security.Identity.UserManager>()
                .AddRoleManager<DDDShell.Security.Identity.RoleManager>()
                .AddSignInManager<DDDShell.Security.Identity.SignInManager>()
                .AddUserStore<DDDShell.Infrastructure.DDDSUserStore>()
                .AddRoleStore<DDDShell.Infrastructure.DDDSRoleStore>()
                .AddEntityFrameworkStores<DDDShell.Context.DDDShellDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(opt =>
            {
                opt.AccessDeniedPath = "/Account/AccessDenied";
                opt.Cookie.Name = "Monchi";
                opt.Cookie.HttpOnly = true;
                opt.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                opt.LoginPath = "/Account/Login";
                opt.LogoutPath = "/Account/Logout";
            });
            #endregion

            #region /// LOCALIZATION
            services.AddLocalization(options => options.ResourcesPath = "Resources"); //se si utilizza dei dizionari per le traduzioni o altro
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[] {
                    new CultureInfo("it-IT"), new CultureInfo("it"),
                    new CultureInfo("en-GB"),new CultureInfo("en-US"),  new CultureInfo("en")
                };
                options.DefaultRequestCulture = new RequestCulture("it");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            #endregion

            #region /// MVC
            services
                .AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization()
                .AddJsonOptions(opt =>
                {
                    opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            #endregion

            #region /// AUTOMAPPER
            IServiceProvider provider = services.BuildServiceProvider();
            IMapper mapper = new MapperConfiguration(c =>
            {
                
            })
            .CreateMapper();
            services.AddSingleton(mapper);
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
