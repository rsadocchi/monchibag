﻿using DDDShell.Domain.SeedWork;
using Microsoft.EntityFrameworkCore.Storage;

namespace DDDShell.Context
{
    public class DbContextTransactionConverter : IDbTransaction
    {
        private readonly IDbContextTransaction _dbContextTransaction;

        public DbContextTransactionConverter(IDbContextTransaction dbContextTransaction)
        {
            _dbContextTransaction = dbContextTransaction;
        }

        public void Commit()
        {
            _dbContextTransaction.Commit();
        }

        public void Dispose()
        {
            _dbContextTransaction.Dispose();
        }

        public void Rollback()
        {
            _dbContextTransaction.Rollback();
        }
    }
}
