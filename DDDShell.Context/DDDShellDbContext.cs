﻿//using DDDShell.Domain.AggregateModels;
using DDDShell.Domain.SeedWork;
using DDDShell.Context.EntityConfigurations;
using DDDShell.Security.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace DDDShell.Context
{
    public class DDDShellDbContext : IdentityDbContext<User, Role, int, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>, IUnitOfWork
    {
        #region DBSets

        #endregion

        #region Constructors
        public DDDShellDbContext() { }
        public DDDShellDbContext(DbContextOptions<DDDShellDbContext> options) : base(options) { }
        #endregion

        #region Override Methods
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=(local); Initial Catalog=DDDShell; Integrated Security=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            /// Carico le configurazioni delle tabelle del DB da generare
            /// che sono state definite nelle classi della cartella "EntityConfigurations"
            modelBuilder.ApplyConfiguration(new SecurityUserConfiguration() { Schema = TableSchema.Security });
            modelBuilder.ApplyConfiguration(new SecurityRoleConfiguration() { Schema = TableSchema.Security });
            modelBuilder.ApplyConfiguration(new SecurityUserClaimConfiguration() { Schema = TableSchema.Security });
            modelBuilder.ApplyConfiguration(new SecurityUserLoginConfiguration() { Schema = TableSchema.Security });
            modelBuilder.ApplyConfiguration(new SecurityRoleClaimConfiguration() { Schema = TableSchema.Security });
            modelBuilder.ApplyConfiguration(new SecurityUserRoleConfiguration() { Schema = TableSchema.Security });
            modelBuilder.ApplyConfiguration(new SecurityUserTokenConfiguration() { Schema = TableSchema.Security });
            modelBuilder.ApplyConfiguration(new SecurityPasswordHistoryConfiguration() { Schema = TableSchema.Security });
        }
        #endregion

        #region Interfaces Implementation
        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
            => (await base.SaveChangesAsync() > 0);


        public IDbTransaction BeginTransaction() => new DbContextTransactionConverter(Database.BeginTransaction());
        #endregion
    }
}
