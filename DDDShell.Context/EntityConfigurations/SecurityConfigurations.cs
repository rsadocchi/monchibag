﻿using DDDShell.Security.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDShell.Context.EntityConfigurations
{
    internal class SecurityUserConfiguration : IEntityTypeConfiguration<User>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(name: nameof(User), schema: Schema);
            builder.Property(e => e.Id).HasColumnName("UserID");
            builder.Property(e => e.Culture).HasColumnType("nvarchar").HasMaxLength(5).IsRequired(false);
            builder.Property(e => e.Avatar).HasColumnType("ntext").IsRequired(false);
        }
    }

    internal class SecurityRoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable(name: nameof(Role), schema: Schema);
            builder.Property(e => e.Id).HasColumnName("RoleID");
        }
    }

    internal class SecurityUserClaimConfiguration : IEntityTypeConfiguration<UserClaim>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<UserClaim> builder)
        {
            builder.ToTable(name: nameof(UserClaim), schema: Schema);
            builder.Property(e => e.UserId).HasColumnName("UserID");
            builder.Property(e => e.Id).HasColumnName("UserClaimID");
            builder.Property(e => e.ClaimType).HasMaxLength(150).IsRequired();
            builder.Property(e => e.ClaimValue).IsRequired();
        }
    }

    internal class SecurityUserLoginConfiguration : IEntityTypeConfiguration<UserLogin>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<UserLogin> builder)
        {
            builder.ToTable(name: nameof(UserLogin), schema: Schema);
            builder.Property(e => e.UserId).HasColumnName("UserID");
        }
    }

    internal class SecurityRoleClaimConfiguration : IEntityTypeConfiguration<RoleClaim>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<RoleClaim> builder)
        {
            builder.ToTable(name: nameof(RoleClaim), schema: Schema);
            builder.Property(e => e.Id).HasColumnName("RoleClaimID");
            builder.Property(e => e.RoleId).HasColumnName("RoleID");
            builder.Property(e => e.ClaimType).HasMaxLength(150).IsRequired();
            builder.Property(e => e.ClaimValue).IsRequired();
        }
    }

    internal class SecurityUserRoleConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable(name: nameof(UserRole), schema: Schema);
            builder.Property(e => e.UserId).HasColumnName("UserID");
            builder.Property(e => e.RoleId).HasColumnName("RoleID");
        }
    }

    internal class SecurityUserTokenConfiguration : IEntityTypeConfiguration<UserToken>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<UserToken> builder)
        {
            builder.ToTable(name: nameof(UserToken), schema: Schema);
            builder.Property(e => e.UserId).HasColumnName("UserID");
        }
    }

    internal class SecurityPasswordHistoryConfiguration : IEntityTypeConfiguration<PasswordHistory>
    {
        public string Schema { get; set; }
        public void Configure(EntityTypeBuilder<PasswordHistory> builder)
        {
            builder.ToTable(name: nameof(PasswordHistory), schema: Schema);
            builder.Property(e => e.UserId).HasColumnName("UserID");
        }
    }
}
