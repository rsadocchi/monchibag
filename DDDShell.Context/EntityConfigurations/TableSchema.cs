﻿
namespace DDDShell.Context.EntityConfigurations
{
    public static class TableSchema
    {
        public static string Default { get; } = "dbo";

        public static string Enum { get; } = "Enum";

        public static string Security { get; } = "Security";
    }
}
