﻿using ElmahCore.Mvc;
using DDDShell.API.Middleware;
using DDDShell.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NLog.Web;
using Swashbuckle.AspNetCore.Swagger;
using System.Text;

namespace DDDShell.API
{
    public class Startup
    {
        private const string SecretKey = "9cDKsnivePrX5ejS2munBvZUHjwmFvWkcrZVDXY4"; // TODO: get this from somewhere secure
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile($"securitysettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"securitysettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            #region /// ELMAH
            services.AddElmah(options =>
            {
                //options.CheckPermissionAction = context => context.User.Identity.IsAuthenticated;
            });
            #endregion

            #region /// CORS
            services.AddCors(o => o.AddPolicy("AllowAll", policy =>
            {
                policy
                    .AllowAnyMethod()
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));
            #endregion

            #region /// CONTEXT
            /// Migrations Assembly reference
            var migrationsAssembly = typeof(Startup).Assembly.GetName().Name;
            services.AddDbContext<DDDShell.Context.DDDShellDbContext>(o =>
                o.UseSqlServer(
                    Configuration.GetConnectionString("DDDShellSecurity_ConnectionString"),
                    opt => opt.MigrationsAssembly(migrationsAssembly)));
            #endregion

            #region /// DEPENDENCY INJECTION
            services.AddScoped<DDDShell.Domain.AggregateModels.ICipherService, DDDShell.Infrastructure.Services.CipherService>();
            services.AddScoped<DDDShell.Domain.AggregateModels.IEmailService, DDDShell.Infrastructure.Services.MailKitEmailService>();
            #endregion

            #region /// IDENTITY
            services.AddIdentity<DDDShell.Security.Identity.User, DDDShell.Security.Identity.Role>(identityOptions =>
            {
                identityOptions.Password.RequireDigit = Configuration.GetValue<bool>("security:identityOptions:Password:RequireDigit");
                identityOptions.Password.RequiredLength = Configuration.GetValue<int>("security:identityOptions:Password:RequiredLength");
                identityOptions.Password.RequiredUniqueChars = Configuration.GetValue<int>("security:identityOptions:Password:RequiredUniqueChars");
                identityOptions.Password.RequireLowercase = Configuration.GetValue<bool>("security:identityOptions:Password:RequireLowercase");
                identityOptions.Password.RequireNonAlphanumeric = Configuration.GetValue<bool>("security:identityOptions:Password:RequireNonAlphanumeric");
                identityOptions.Password.RequireUppercase = Configuration.GetValue<bool>("security:identityOptions:Password:RequireUppercase");
                identityOptions.User.RequireUniqueEmail = Configuration.GetValue<bool>("security:identityOptions:User:RequireUniqueEmail");
                identityOptions.SignIn.RequireConfirmedEmail = Configuration.GetValue<bool>("security:identityOptions:SignIn:RequireConfirmedEmail");
                identityOptions.SignIn.RequireConfirmedPhoneNumber = Configuration.GetValue<bool>("security:identityOptions:SignIn:RequireConfirmedPhoneNumber");
            })
                .AddUserManager<Security.Identity.UserManager>()
                .AddRoleManager<Security.Identity.RoleManager>()
                .AddSignInManager<Security.Identity.SignInManager>()
                .AddUserStore<DDDShell.Infrastructure.DDDSUserStore>()
                .AddRoleStore<DDDShell.Infrastructure.DDDSRoleStore>()
                .AddEntityFrameworkStores<DDDShellDbContext>()
                .AddDefaultTokenProviders();
            #endregion

            #region /// MVC
            services
                .AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization()
                .AddJsonOptions(opt =>
                {
                    opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });
            #endregion

            #region /// SWAGGER
            services
                .AddSwaggerGen(opt =>
                {
                    opt.SwaggerDoc("v1", new Info { Title = "DDDShell API", Version = "V.1", });
                    opt.AddSecurityDefinition("Bearer", new ApiKeyScheme
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = "header",
                        Type = "apiKey"
                    });
                    opt.OperationFilter<Swashbuckle.AspNetCore.Filters.AddAuthTokenHeaderParameter>();
                });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
