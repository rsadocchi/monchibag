﻿using DDDShell.Security.Identity;
using Microsoft.AspNetCore.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DDDShell.API.Middleware
{
    public class CultureMiddleware
    {
        private readonly RequestDelegate _next;


        public CultureMiddleware(RequestDelegate next)
        {
            _next = next;
        }


        public async Task Invoke(HttpContext context, UserManager userManager)
        {
            if (userManager != null)
            {
                var user = await userManager.GetUserAsync(context.User);
                if (user != null)
                {
                    Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(user.Culture);
                    Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(user.Culture);
                }
            }
            await _next(context);
        }
    }
}