﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace DDDShell.API.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;


        public ErrorHandlingMiddleware(
            RequestDelegate next,
            ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }


        public async Task Invoke(HttpContext context/* other scoped dependencies */)
        {
            try
            {
                await _next(context);
            }
            /// Logging errore di validazione fluent
            catch (ValidationException e)
            {
                _logger.LogInformation(e, "ERROR_HANDLING_MIDDLEWARE ValidationException // Fluent validation exception");

                foreach (var err in e.Errors) _logger.LogInformation($"{err.PropertyName}: {err.ErrorCode} {err.Severity} | {err.ErrorMessage}", "DbEntityValidationException");

                await HandleErrorAsync(context, e);
            }
            /// Logging errore di validazione record database
//            catch (DbEntityValidationException e)
//            {
//#if (DEBUG)
//                Debugger.Break();
//#endif
//                _logger.LogCritical(e, "ERROR_HANDLING_MIDDLEWARE DbEntityValidationException // Fatal database validation exception");

//                var errorList = e.EntityValidationErrors.SelectMany(x => x.ValidationErrors).OrderByDescending(t => t.PropertyName);
//                foreach (var err in errorList) _logger.LogCritical($"{err.PropertyName}: {err.ErrorMessage}", "DbEntityValidationException");

//                await HandleErrorAsync(context, e);
//            }
            /// Logging errore generico
            catch (Exception e)
            {
                _logger.LogCritical(e, "ERROR_HANDLING_MIDDLEWARE Exception // Generic fatal exception");

                await HandleErrorAsync(context, e);
            }
        }
        private static Task HandleErrorAsync(HttpContext context, Exception e)
        {
            string responseJsonContent = "";
            HttpStatusCode responseStatusCode = HttpStatusCode.InternalServerError;
            if (e is ValidationException) responseStatusCode = HttpStatusCode.BadRequest;

#if (DEBUG)
            responseJsonContent = JsonConvert.SerializeObject(e, Formatting.Indented);
#elif (RELEASE)
            responseJsonContent = JsonConvert.SerializeObject(GetOnlySomeFieldsFromExceptionObject(e));
#endif

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)responseStatusCode;
            return context.Response.WriteAsync(responseJsonContent);

            object GetOnlySomeFieldsFromExceptionObject(Exception exc)
            {
                if (exc == null) return null;
                // Se l'eccezione che sto provando a serializzare non è una Validation
                // allora vado a ricercare una ValidationException nella InnerException
                if (!(exc is ValidationException)) return GetOnlySomeFieldsFromExceptionObject(exc.InnerException);
                return new
                {
                    exc.Message,
                    exc.Data,
                    InnerException = GetOnlySomeFieldsFromExceptionObject(exc.InnerException),
                    exc.HelpLink,
                    exc.HResult
                };
            }
        }
    }
}