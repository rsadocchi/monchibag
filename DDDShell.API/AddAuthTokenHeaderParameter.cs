﻿using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Swashbuckle.AspNetCore.Filters
{
    public class AddAuthTokenHeaderParameter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            operation.Parameters.Add(new HeaderParameter()
            {
                Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {Token}\"",
                Name = "Authorization",
                In = "header",
                Type = "apiKey",
                Required = false
            });
        }
    }

    class HeaderParameter : NonBodyParameter { }
}