﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DDDShell.Common.InternalServices
{
    internal sealed class FormatterService : IFormatProvider, ICustomFormatter
    {
        readonly CultureInfo culture;
        const int ACCT_LENGTH = 12;

        string HandleOtherFormats(string format, object arg)
        {
            if (arg is IFormattable)
                return ((IFormattable)arg).ToString(format, culture);
            else if (arg != null)
                return arg.ToString();
            else
                return String.Empty;
        }

        public FormatterService()
        {
            culture = CultureInfo.CurrentCulture;
        }

        public FormatterService(CultureInfo cultureInfo)
        {
            if (cultureInfo == null)
                cultureInfo = CultureInfo.CurrentUICulture;
            culture = cultureInfo;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (arg.GetType() != typeof(Int64))
                try
                {
                    return HandleOtherFormats(format, arg);
                }
                catch (FormatException e)
                {
                    throw new FormatException(String.Format("The format of '{0}' is invalid.", format), e);
                }

            string ufmt = format.ToUpper(CultureInfo.InvariantCulture);
            if (!(ufmt == "H" || ufmt == "I"))
                try
                {
                    return HandleOtherFormats(format, arg);
                }
                catch (FormatException e)
                {
                    throw new FormatException(String.Format("The format of '{0}' is invalid.", format), e);
                }

            string result = arg.ToString();

            if (result.Length < ACCT_LENGTH)
                result = result.PadLeft(ACCT_LENGTH, '0');
            if (result.Length > ACCT_LENGTH)
                result = result.Substring(0, ACCT_LENGTH);

            if (ufmt == "I")
                return result;
            else
                return result.Substring(0, 5) + "-" + result.Substring(5, 3) + "-" + result.Substring(8);
        }

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;
            else
                return null;
        }

    }

    internal sealed class ValidatorService
    {
        #region E-MAIL
        private bool IsInvalid { get; set; }
        string DomainMapper(Match match)
        {
            var idnMapping = new IdnMapping();
            var domainName = match.Groups[2].Value;
            try
            {
                domainName = idnMapping.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                IsInvalid = true;
            }
            return match.Groups[1].Value + domainName;
        }

        /// <summary>
        /// Verify a mail address
        /// </summary>
        public bool MailAddress(string address)
        {
            IsInvalid = false;
            if (string.IsNullOrWhiteSpace(address))
                return false;
            else
            {
                try
                {
                    address = Regex.Replace(address, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));
                }
                catch (RegexMatchTimeoutException)
                {
                    return false;
                }

                if (IsInvalid)
                    return false;
                else
                {
                    try
                    {
                        return Regex.IsMatch(address, @"([a-zA-Z0-9\-\_\.]+)\@([a-zA-Z0-9\-\_\.]{3,})\.([a-zA-Z]{2,4})", RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
                    }
                    catch (RegexMatchTimeoutException)
                    {
                        return false;
                    }
                }
            }
        }
        #endregion
    }
}
