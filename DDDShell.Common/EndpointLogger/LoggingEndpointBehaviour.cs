﻿using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace DDDShell.Common.EndpointLogger
{
    public class LoggingEndpointBehaviour : IEndpointBehavior
    {
        readonly IClientMessageInspector _messageInspector;


        public LoggingEndpointBehaviour(IClientMessageInspector messageInspector)
        {
            _messageInspector = messageInspector ?? throw new ArgumentNullException(nameof(messageInspector));
        }


        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) { clientRuntime.ClientMessageInspectors.Add(_messageInspector); }
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }
        public void Validate(ServiceEndpoint endpoint) { }
    }
}