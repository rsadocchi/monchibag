﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Xml;

namespace DDDShell.Common.EndpointLogger
{
    public class LoggingMessageInspector : IClientMessageInspector
    {
        readonly ILogger<LoggingMessageInspector> _logger;


        public LoggingMessageInspector(ILogger<LoggingMessageInspector> logger)
        {
            _logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
        }


        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            using (var buffer = request.CreateBufferedCopy(int.MaxValue))
            {
                var document = GetDocument(buffer.CreateMessage());
                _logger.LogInformation($"LoggingMessageInspector{Environment.NewLine}{document.OuterXml}");
                request = buffer.CreateMessage();
                return null;
            }
        }
        
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            using (var buffer = reply.CreateBufferedCopy(int.MaxValue))
            {
                var document = GetDocument(buffer.CreateMessage());
                _logger.LogInformation($"LoggingMessageInspector{Environment.NewLine}{document.OuterXml}");
                reply = buffer.CreateMessage();
            }
        }

        private XmlDocument GetDocument(Message request)
        {
            XmlDocument document = new XmlDocument();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                // write request to memory stream
                XmlWriter writer = XmlWriter.Create(memoryStream);
                request.WriteMessage(writer);
                writer.Flush();
                memoryStream.Position = 0;

                // load memory stream into a document
                document.Load(memoryStream);
            }
            return document;
        }
    }
}