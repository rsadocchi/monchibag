﻿using DDDShell.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDShell.Common.Currency
{
    public static partial class Extensions
    {
        /// <summary>
        /// Restituisce una stringa che rappresenta la trascrizione dell'importo passato come argomento. 
        /// I decimali non vengono trascritti (es.: duemilaseicentotrentaquattro/22).
        /// </summary>
        public static string ToStringMoney(this decimal val, string culture = null)
        {
            if (string.IsNullOrEmpty(culture)) culture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;

            long numero = (long)decimal.Truncate(val);
            int decimali = (int)(Math.Round(val - numero, 2) * 100);
            long mod, i;

            var unita = typeof(UnitiesEnum).GetLocalizedValues().Where(t => t.CultureCode.Equals(culture, StringComparison.InvariantCultureIgnoreCase)).Select(t => t.Value).ToArray();
            var decine = typeof(DozensEnum).GetLocalizedValues().Where(t => t.CultureCode.Equals(culture, StringComparison.InvariantCultureIgnoreCase)).Select(t => t.Value).ToArray();
            var altro = typeof(HundredsAndOverEnum).GetLocalizedValues().Where(t => t.CultureCode.Equals(culture, StringComparison.InvariantCultureIgnoreCase)).Select(t => new
            {
                EnumValue = (int)t.EnumValue,
                Localized = t.Value
            })
            .ToDictionary(k => k.EnumValue, v => v.Localized);

            string rtn = "";

            if (numero >= 0 && numero < 20)
                rtn = unita[numero];
            else
            {
                if (numero < 100)
                {
                    mod = numero % 10;
                    i = numero / 10;
                    switch (mod)
                    {
                        case 0:
                            rtn = decine[i];
                            break;
                        case 1:
                            rtn = decine[i].Substring(0, decine[i].Length - 1) + unita[mod];
                            break;
                        case 8:
                            rtn = decine[i].Substring(0, decine[i].Length - 1) + unita[mod];
                            break;
                        default:
                            rtn = decine[i] + unita[mod];
                            break;
                    }
                }
                else
                {
                    if (numero < 1000)
                    {
                        mod = numero % 100;
                        i = numero / 100;
                        switch (i)
                        {
                            case 1:
                                rtn = altro[100];
                                break;
                            default:
                                rtn = unita[i] + altro[100];
                                break;
                        }
                        if (mod > 0)
                            rtn += ToStringMoney(mod, culture);
                    }
                    else
                    {
                        if (numero < 10000)
                        {
                            mod = numero % 1000;
                            i = (numero - mod) / 1000;
                            switch (i)
                            {
                                case 1:
                                    rtn = altro[1000];
                                    break;
                                default:
                                    rtn = string.Format(altro[10000], unita[i]);
                                    break;
                            }
                            if (mod > 0)
                                rtn += ToStringMoney(mod, culture);
                        }
                        else
                        {
                            if (numero < 1000000)
                            {
                                mod = numero % 1000;
                                i = (numero - mod) / 1000;
                                switch ((numero - mod) / 1000)
                                {
                                    default:
                                        if (i < 20)
                                        {
                                            rtn = string.Format(altro[10000], unita[i]);
                                        }
                                        else
                                        {
                                            rtn = string.Format(altro[10000], ToStringMoney(i, culture));
                                        }
                                        break;
                                }
                                if (mod > 0)
                                    rtn += ToStringMoney(mod, culture);
                            }
                            else
                            {
                                if (numero < 1000000000)
                                {
                                    mod = numero % 1000000;
                                    i = (numero - mod) / 1000000;
                                    switch (i)
                                    {
                                        case 1:
                                            string unit = culture.Equals("it-IT", StringComparison.InvariantCultureIgnoreCase) ? unita[1].Substring(0, unita[1].Length - 1) : unita[1];
                                            rtn = $"{unit}{altro[1000000]}";
                                            break;
                                        default:
                                            rtn = $"{ToStringMoney(i, culture)}{altro[2000000]}";
                                            break;
                                    }
                                    if (mod > 0)
                                        rtn += ToStringMoney(mod, culture);
                                }
                                else
                                {
                                    if (numero < 1000000000000)
                                    {
                                        mod = numero % 1000000000;
                                        i = (numero - mod) / 1000000000;
                                        switch (i)
                                        {
                                            case 1:
                                                rtn = $"{unita[1]}{altro[1000000000]}";
                                                break;

                                            default:
                                                rtn = $"{ToStringMoney(i, culture)}{altro[2000000000]}";
                                                break;
                                        }
                                        if (mod > 0)
                                            rtn += ToStringMoney(mod, culture);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return string.Format($"{rtn}{(decimali > 0 ? string.Format(",{0:00}", decimali) : "")}");
        }
    }
}
