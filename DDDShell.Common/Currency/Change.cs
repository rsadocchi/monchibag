﻿using System;
using System.Xml;

namespace DDDShell.Common.Currency
{
    public class Change
    {
        /// <summary>
        /// Contiene il riferimento alla valuta di base utilizzata come riferimento per i cambi.
        /// </summary>
        public string Currency_Base => "EUR";

        /// <summary>
        /// Ottiene il cambio relativao all'attuale valuta impostata rispetto a quella di base.
        /// </summary>
        public decimal Get_ActualChangeRate => _changeRate;
        private decimal _changeRate = 1m;

        /// <summary>
        /// Imposta o recupera l'attuale valuta (formato internazionale di 3 lettere, es. EUR, USD, GPB...).
        /// </summary>
        public string Currency
        {
            get => _currency;
            set
            {
                if (value.Length == 3)
                    _currency = value.ToUpper();
                else
                    throw new Exception("Invalid currency format!!! Use 3 chars global format:\n\t euro => EUR\n\t US dollar => USD\n\t GB pound => GPB\n\t ...");
            }
        }
        private string _currency = "EUR";

        /// <summary>
        /// Ottiene la url ufficiale da cui vengono recuperati i cambi aggiornati.
        /// </summary>
        public string UrlChange => _url;
        private string _url = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

        /// <summary>
        /// Recupera il cambio della valuta specificata senza modificare le impostazioni di base della classe.
        /// </summary>
        public decimal RetriveChangeRate(string currency)
        {
            decimal rtn = 0m;
            bool found = false;

            XmlDocument doc = new XmlDocument();
            doc.Load(_url);

            foreach (XmlNode nodes in doc.DocumentElement.ChildNodes)
                foreach (XmlNode node in nodes)
                    if (node.Name.Equals("Cube"))
                        foreach (XmlNode nd in node)
                            if (nd.Attributes["currency"].Value == currency.ToUpper())
                            {
                                found = true;
                                rtn = Convert.ToDecimal(nd.Attributes["rate"].Value.Replace(",", "."));
                            }

            if (!found)
                throw new Exception("Currency not found!!!");
            else
                return rtn;
        }

        /// <summary>
        /// Cambia la valuta attualmente utilizzata
        /// </summary>
        /// <param name="NewCurrency">La nuova valuta da utilizzare</param>
        public void ChangeActCurrencyAndRate(string NewCurrency)
        {
            decimal newRate = RetriveChangeRate(NewCurrency);

            _currency = NewCurrency;
            _changeRate = newRate;
        }
    }
}
