﻿using DDDShell.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDDShell.Common.Currency
{
    public enum UnitiesEnum
    {
        [Localizer("zero","en-US", 0), Localizer("zero","it-IT", 0)] Zero = 0,
        [Localizer("one","en-US", 1), Localizer("uno","it-IT", 1)] One = 1,
        [Localizer("two","en-US", 2), Localizer("due","it-IT", 2)] Two = 2,
        [Localizer("three","en-US", 3), Localizer("tre","it-IT", 3)] Three = 3,
        [Localizer("four","en-US", 4), Localizer("quattro","it-IT", 4)] Four = 4,
        [Localizer("five","en-US", 5), Localizer("cinque","it-IT", 5)] Five = 5,
        [Localizer("six","en-US", 6), Localizer("sei","it-IT", 6)] Six = 6,
        [Localizer("seven","en-US", 7), Localizer("sette","it-IT", 7)] Seven = 7,
        [Localizer("eight","en-US", 8), Localizer("otto","it-IT", 8)] Eight = 8,
        [Localizer("nine","en-US", 9), Localizer("nove","it-IT", 9)] Nine = 9,
        [Localizer("ten","en-US", 10), Localizer("dieci","it-IT", 10)] Ten = 10,
        [Localizer("eleven","en-US", 11), Localizer("undici","it-IT", 11)] Eleven = 11,
        [Localizer("twelve","en-US", 12), Localizer("dodici","it-IT", 12)] Twelve = 12,
        [Localizer("thirteen","en-US", 13), Localizer("tredici","it-IT", 13)] Thirteen = 13,
        [Localizer("fourteen", "en-US", 14), Localizer("quattordici","it-IT", 14)] Fourteen = 14,
        [Localizer("fifteen", "en-US", 15), Localizer("quindici","it-IT", 15)] Fifteen = 15,
        [Localizer("sixteen", "en-US", 16), Localizer("sedici","it-IT", 16)] Sixteen = 16,
        [Localizer("seventeen", "en-US", 17), Localizer("diciassette","it-IT", 17)] Seventeen = 17,
        [Localizer("eighteen", "en-US", 18), Localizer("diciotto","it-IT", 18)] Eighteen = 18,
        [Localizer("nineteen", "en-US", 19), Localizer("diciannove", "it-IT", 19)] Nineteen =19
    }

    public enum DozensEnum
    {
        [Localizer("", "en-US", 0), Localizer("", "it-IT", 0)] Empty = 0,
        [Localizer("ten", "en-US", 10), Localizer("dieci", "it-IT", 10)] Ten = 10,
        [Localizer("twenty", "en-US", 20), Localizer("venti", "it-IT", 20)] Twenty = 20,
        [Localizer("thirty", "en-US", 30), Localizer("trenta", "it-IT", 30)] Thirty = 30,
        [Localizer("fourty", "en-US", 40), Localizer("quaranta", "it-IT", 40)] Fourty = 40,
        [Localizer("fifty", "en-US", 50), Localizer("cinquanta", "it-IT", 50)] Fifty = 50,
        [Localizer("sixty", "en-US", 60), Localizer("sessanta", "it-IT", 60)] Sixty = 60,
        [Localizer("seventy", "en-US", 70), Localizer("settanta", "it-IT", 70)] Seventy = 70,
        [Localizer("eighty", "en-US", 80), Localizer("ottanta", "it-IT", 80)] Eighty = 80,
        [Localizer("ninety", "en-US", 90), Localizer("novanta", "it-IT", 90)] Ninety = 90
    }

    public enum HundredsAndOverEnum
    {
        [Localizer("", "en-US", 0), Localizer("", "it-IT", 0)] Empty = 0,
        [Localizer("hundred", "en-US", 100), Localizer("cento", "it-IT", 100)] Hundred = 100,
        [Localizer("thousand", "en-US", 1000), Localizer("mille", "it-IT", 1000)] Thousand = 1000,
        [Localizer("{0}-thousend", "en-US", 10000), Localizer("{0}mila", "it-IT", 10000)] UnitThousend = 10000,
        [Localizer("million", "en-US", 1000000), Localizer("milione", "it-IT", 1000000)] Million = 1000000,
        [Localizer("millions", "en-US", 2000000), Localizer("milioni", "it-IT", 2000000)] Million2 = 2000000,
        [Localizer("billion", "en-US", 1000000000), Localizer("miliardo", "it-IT", 1000000000)] Billion = 1000000000,
        [Localizer("billions", "en-US", 2000000000), Localizer("miliardi", "it-IT", 2000000000)] Billions = 2000000000
    }
}
