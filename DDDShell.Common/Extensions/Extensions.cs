﻿using DDDShell.Common.InternalServices;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DDDShell.Common.Extensions
{
    public static partial class Extensions
    {
        public static IEnumerable<string> SpecialChars => new string[] { "£", "$", "&", "@", ".", "-", "_", "!", "=", "à", "é", "è", "ì", "ò", "ù" };

        public static bool ContainNumbers(this string source) => Regex.IsMatch(source, @"\d");

        public static bool ContainsUpperCase(this string source) => Regex.IsMatch(source, @"[A-Z]");

        public static bool ContainsSpecialChar(this string source, string pattern = null) => Regex.IsMatch(source, (pattern ?? $"[{string.Join("", SpecialChars)}]"));

        public static string ToFormattedMoneyString(this decimal val, CultureInfo culture = null)
        {
            var rtn = string.Empty;
            if (culture == null) culture = CultureInfo.CurrentUICulture;
            rtn = string.Format(new FormatterService(culture), "{0:C2}", val);
            return rtn;
        }

        public static bool IsValidEmailAddress(this string email) => (new ValidatorService()).MailAddress(email);

        #region Numbers
        public static bool IsEven(this int num) => (num % 2 == 0 ? true : false);
        public static bool IsEven(this long num) => (num % 2 == 0 ? true : false);
        public static bool IsEven(this double num) => (num % 2 == 0 ? true : false);
        public static bool IsEven(this float num) => (num % 2 == 0 ? true : false);
        public static bool IsEven(this short num) => (num % 2 == 0 ? true : false);
        public static bool IsEven(this decimal num) => (num % 2 == 0 ? true : false);
        #endregion

        public static string GeneratePassword(int length = 8)
        {
            Random rnd = new Random();
            string password = string.Empty;
            while(password.Length < length)
            {
                int c = rnd.Next(33, 123);
                if ((c >= 48 && c <= 57) || /* 0-9 */
                    (c >= 65 && c <= 90) || /* A-Z */
                    (c >= 97 && c <= 122) || /* a-z */
                    c == 33 || (c >= 36 && c <= 38) || c == 95 /* ! # $ % & _ */)
                    password += (char)c;
            }
            return password;
        }

        public static string ToMD5Hash(this string source)
        {
            if (string.IsNullOrWhiteSpace(source)) return null;
            var sb = new StringBuilder();
            var csp = new MD5CryptoServiceProvider();
            var bytes = csp.ComputeHash(Encoding.UTF8.GetBytes(source));
            bytes.ToList().ForEach(b => { sb.Append(b.ToString("x2")); });
            return sb.ToString();
        }

        #region String to Byte array and Back
        public static byte[] ToByteArray(this Stream stream)
        {
            byte[] output = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = stream.Read(output, 0, output.Length)) > 0)
                    ms.Write(output, 0, read);
                return ms.ToArray();
            }
        }

        public static byte[] ToByteArray(this string source, Encoding encoding = null)
        {
            if (encoding == null) encoding = Encoding.UTF8;
            return encoding.GetBytes(source);
        }

        public static Stream ToStream(this string source, Encoding encoding = null)
        {
            byte[] byteArray = source.ToByteArray(encoding);
            return new MemoryStream(byteArray);
        }

        public static async Task<string> ToStringBackAsync(this byte[] source)
        {
            string text = null;
            using (var ms = new MemoryStream(source))
            using (var sr = new StreamReader(ms))
                text = await sr.ReadToEndAsync();
            return await Task.FromResult(text);
        }

        public static async Task<string> ToStringBackAsync(this Stream source)
        {
            string text = null;
            using (var sr = new StreamReader(source))
                text = await sr.ReadToEndAsync();
            return await Task.FromResult(text);
        }
        #endregion

        public static int LevenshteinDistance(this string source, string compare)
        {
            int n = source.Length;
            int m = compare.Length;
            int[,] d = new int[n + 1, m + 1];
            // Step 1
            if (n == 0) return m;
            if (m == 0) return n;
            //// Step 2
            //for (int i = 0; i <= n; d[i, 0] = i++) { }
            //for (int j = 0; j <= m; d[0, j] = j++) { }
            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (compare[j - 1] == source[i - 1]) ? 0 : 1;
                    // Step 6
                    d[i, j] = Math.Min(Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1), d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }

        public static string RemoveSpecialCharacters(this string @string, params char[] allowed)
        {
            StringBuilder sb = new StringBuilder();
            @string = @string.Replace("&nbsp;", "");
            foreach (char c in @string)
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_' || allowed.Contains(c)) sb.Append(c);
            return Regex.Replace(sb.ToString(), @"\s+", " ").Trim();
        }
    }
}
