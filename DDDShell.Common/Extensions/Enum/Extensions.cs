﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDShell.Common.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Restituisce una collezione iterabile dei valori della enum
        /// </summary>
        public static IEnumerable<TSource> GetValues<TSource>(this TSource @enum)
            where TSource : struct, IConvertible
        {
            if (!typeof(TSource).IsEnum) return null;
            return System.Enum.GetValues(typeof(TSource)).Cast<TSource>();
        }

        /// <summary>
        /// Restituisce una collezione iterabile della rappresentazione in stringa dei membri della enum
        /// </summary>
        public static IEnumerable<string> GetNames<TSource>(this TSource @enum)
            where TSource : struct, IConvertible
        {
            if (!typeof(TSource).IsEnum) return null;
            return System.Enum.GetNames(typeof(TSource)).AsEnumerable();
        }

        /// <summary>
        /// Restituisce il membro corrispondente della enum al valore passato come parametro
        /// </summary>
        public static string GetName<TSource>(this TSource @enum, object value)
            where TSource : struct, IConvertible
        {
            if (!typeof(TSource).IsEnum) return null;
            return System.Enum.GetName(typeof(TSource), value);
        }

        public static TValue GetCustomValue<TSource, TValue>(this TSource source)
            where TSource : struct, IConvertible
            where TValue : Type
        {
            if (!typeof(TSource).IsEnum) return null;
            var memberInfo = typeof(TSource).GetField(System.Enum.GetName(typeof(TSource), source));
            var value = (CustomAttribute)Attribute.GetCustomAttribute(memberInfo, typeof(CustomAttribute));
            if (value == null) return null;
            if (value.ValueType != null && value.ValueType != typeof(TValue)) throw new InvalidCastException($"Impossible cast of {value.ValueType.Name} in {(typeof(TValue)).Name}");
            return (TValue)value.Value;
        }

    }
}
