﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDDShell.Common.Extensions
{
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class CustomAttribute : Attribute
    {
        public object Value { get; private set; }
        public Type ValueType { get; private set; }

        public CustomAttribute(object value, Type valueType = null)
        {
            Value = value;
            ValueType = valueType;
        }
    }
}
