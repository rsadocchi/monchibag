﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DDDShell.Common.Extensions
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
    public class LocalizerAttribute : Attribute
    {
        public string Value { get; set; } = "";
        public string CultureCode { get; set; } = "it-IT";
        public object EnumValue { get; set; } = null;
        public LocalizerAttribute(string value, string culture)
        {
            Value = value;
            CultureCode = culture;
        }
        public LocalizerAttribute(string value, string culture, object enumValue)
        {
            Value = value;
            CultureCode = culture;
            EnumValue = enumValue;
        }
    }

    public static partial class Extensions
    {
        //public static string GetLocalizedValue(this Enum source, string culture)
        //{
        //    var ci = new System.Globalization.CultureInfo(culture);
        //    return source.GetLocalizedValue(ci);
        //}

        //public static string GetLocalizedValue(this Enum source, System.Globalization.CultureInfo culture)
        //{
        //    var test = source
        //        .GetType()
        //        .GetFields()
        //        .SelectMany(m => m.GetLocalizedValues())
        //        .Where(m => new System.Globalization.CultureInfo(m.CultureCode) == culture).ToList();



        //    var info = typeof(Enum).GetField(System.Enum.GetName(typeof(Enum), source));
        //    var values = info.GetLocalizedValues()
        //        .Where(t => new System.Globalization.CultureInfo(t.CultureCode) == culture)
        //        .ToList();
        //    return values.Select(o => o.Value).FirstOrDefault();
        //}

        public static string GetLocalizedValue(this Type source, string culture)
        {
            var ci = new System.Globalization.CultureInfo(culture);
            return source.GetLocalizedValue(ci);
        }

        public static string GetLocalizedValue(this Type source, System.Globalization.CultureInfo culture)
        {
            var values = source.GetLocalizedValues()
                .Where(t => new System.Globalization.CultureInfo(t.CultureCode) == culture)
                .ToList();
            return values.Select(o => o.Value).FirstOrDefault();
        }

        public static IEnumerable<LocalizerAttribute> GetLocalizedValues(this Type source)
            => source
                .GetFields()
                .Where(m => m.GetCustomAttributes(typeof(LocalizerAttribute), false).Count() > 0)
                .SelectMany(m => m.GetLocalizedValues());
        
        public static IEnumerable<LocalizerAttribute> GetLocalizedValues(this System.Reflection.FieldInfo info)
            => ((LocalizerAttribute[])info.GetCustomAttributes(typeof(LocalizerAttribute), false));

        public static TValue GetMemberFromLocalizedValue<TSource, TValue>(this Type source, string localizedValue, string culture)
            where TSource : struct, IConvertible
        {
            if (!typeof(TSource).IsEnum) throw new ArgumentException($"{nameof(source)} is not Enum type.");
            return (TValue)
                source.GetLocalizedValues()
                    .Where(t => t.CultureCode.Equals(culture, StringComparison.InvariantCultureIgnoreCase) && t.Value.Equals(localizedValue, System.StringComparison.InvariantCultureIgnoreCase))
                    .Select(t => t.EnumValue)
                    .FirstOrDefault();
        }
    }
}
