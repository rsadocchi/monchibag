﻿using DDDShell.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDShell.Common.Calendar
{
    public class CalendarElementViewModel
    {
        private DateTime _firstDay { get; set; }

        public MonthsEnum Month { get; private set; }
        public int NumberOfDays { get { return Month.GetTotalDays(_firstDay.Year); } }
        public int NumberOfFestivities { get { return Month.GetMonthFestivity(_firstDay.Year); } }
        public int WorkingDays { get { return NumberOfDays - NumberOfFestivities; } }
        public IEnumerable<int> Weeknumbers { get { return Month.GetMonthWeekNumber(_firstDay.Year); } }

        public CalendarElementViewModel(int year, MonthsEnum month)
        {
            Month = month;
            _firstDay = new DateTime(year, (int)month, 1, 0, 0, 0);
        }

    }

    public class CalendarViewModel
    {
        private DateTime _firstDay { get; set; }

        public IList<CalendarElementViewModel> Months { get; set; }
        public bool IsLeap { get { return DateTime.IsLeapYear(_firstDay.Year); } }
        public IEnumerable<DateTime> Festivities { get { return _firstDay.Year.GetFestivityInYear(); } }

        public CalendarViewModel(int year)
        {
            _firstDay = new DateTime(year, 1, 1);
            Months = new List<CalendarElementViewModel>();
            ((MonthsEnum)1).GetValues().ToList().ForEach(m => { Months.Add(new CalendarElementViewModel(year: year, month: m)); });
        }
    }
}
