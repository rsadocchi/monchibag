﻿using DDDShell.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDDShell.Common.Calendar
{
    public enum DaysOfWeekEnum
    {
        [Localizer("Sunday", "en-US", 1), Localizer("Domenica", "it-IT", 7)] Sunday = 1,
        [Localizer("Monday", "en-US", 2), Localizer("Lunedi", "it-IT", 1)] Monday,
        [Localizer("Tuesday", "en-US", 3), Localizer("Martedi", "it-IT", 2)] Tuesday,
        [Localizer("Wednesday", "en-US", 4), Localizer("Mercoledi", "it-IT", 3)] Wednesday,
        [Localizer("Thursday", "en-US", 5), Localizer("Giovedi", "it-IT", 4)] Thursday,
        [Localizer("Friday", "en-US", 6), Localizer("Venerdi", "it-IT", 5)] Friday,
        [Localizer("Saturday", "en-US", 7), Localizer("Sabato", "it-IT", 6)] Saturday
    }

    public enum MonthsEnum
    {
        [Localizer("Janary", "en-US"), Localizer("Gennaio", "it-IT")] January = 1,
        [Localizer("February", "en-US"), Localizer("Febbraio", "it-IT")] February,
        [Localizer("March", "en-US"), Localizer("Marzo", "it-IT")] March,
        [Localizer("April", "en-US"), Localizer("Aprile", "it-IT")] April,
        [Localizer("May", "en-US"), Localizer("Maggio", "it-IT")] May,
        [Localizer("June", "en-US"), Localizer("Giugno", "it-IT")] June,
        [Localizer("July", "en-US"), Localizer("Luglio", "it-IT")] July,
        [Localizer("August", "en-US"), Localizer("Agosto", "it-IT")] August,
        [Localizer("September", "en-US"), Localizer("Settembre", "it-IT")] September,
        [Localizer("October", "en-US"), Localizer("Ottobre", "it-IT")] October,
        [Localizer("November", "en-US"), Localizer("Novembre", "it-IT")] November,
        [Localizer("December", "en-US"), Localizer("Dicembre", "it-IT")] December
    }

    public sealed class Calendar
    {

    }


}
