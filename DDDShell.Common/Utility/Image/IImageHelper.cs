﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DDDShell.Common.Utility
{
    public interface IImageHelper
    {
        Task<System.Drawing.Bitmap> ResizeAsync(System.Drawing.Image source, int? maxWidth = null, int? maxHeight = null);

        Task<string> GetBase64Async(string filePath);
        Task<string> GetBase64Async(System.Drawing.Image source);
        Task<string> GetBase64Async(byte[] source);
        Task<string> GetBase64Async(MemoryStream source);
    }
}
