﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDDShell.ViewModels.Options
{
    public class EmailOptions
    {
        public string FromName { get; set; }
        public string FromAddress { get; set; }

        public string ToAddress { get; set; }

        public string LocalDomain { get; set; }
        public string DefaultBcc { get; set; }

        public string MailServerAddress { get; set; }
        public int MailServerPort { get; set; }
        public bool UseSSL { get; set; }
        public bool UseTLS { get; set; }

        public string UserId { get; set; }
        public string UserPassword { get; set; }

        public bool Debug { get; set; }
        public string DebugDefaultTo { get; set; }
        public string DebugAllowedAddresses { get; set; }
        public string ErrorTo { get; set; }
    }
}