﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDDShell.Domain.AggregateModels
{
    public interface IEmailService
    {
        Task SendEmailAsync(string to, string subject, string body, IEnumerable<string> attachments = null, IEnumerable<string> attachmentNames = null, string from = null, string fromName = null, string cc = null, string bcc = null);
    }
}