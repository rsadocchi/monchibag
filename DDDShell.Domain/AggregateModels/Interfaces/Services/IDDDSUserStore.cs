﻿using DDDShell.Security.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DDDShell.Domain.AggregateModels
{
    public interface IDDDSUserStore<TUser> : Microsoft.AspNetCore.Identity.IUserStore<TUser> where TUser : class
    {
        Task<IList<User>> GetUsersForClaimsIncludeRoleClaimsAsync(Claim[] claims, CancellationToken cancellationToken = default(CancellationToken));
        Task<IList<Role>> GetRolesForClaimsAsync(Claim[] claims, CancellationToken cancellationToken = default(CancellationToken));
    }
}