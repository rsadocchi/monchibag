﻿namespace DDDShell.Domain.AggregateModels
{
    public interface IDDDSRoleStore<TUser> : Microsoft.AspNetCore.Identity.IRoleStore<TUser> where TUser : class { }
}