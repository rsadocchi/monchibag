﻿
namespace DDDShell.Domain.AggregateModels
{
    public interface ICipherService
    {
        string Decrypt(string cipherText, bool urlEncode = false);
        string Encrypt(string input, bool urlEncode = false);
    }
}