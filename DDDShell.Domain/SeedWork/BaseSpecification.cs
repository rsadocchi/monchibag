﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DDDShell.Domain.SeedWork
{
    public abstract class BaseSpecification<T> : ISpecification<T>
    {
        public BaseSpecification() { Criteria = (t => true); }
        public BaseSpecification(Expression<Func<T, bool>> criteria) { Criteria = criteria; }


        public Expression<Func<T, bool>> Criteria { get; internal set; }
        public List<Expression<Func<T, object>>> Includes { get; } = new List<Expression<Func<T, object>>>();
        public List<string> IncludeStrings { get; } = new List<string>();


        public virtual void SetCriteria(Expression<Func<T, bool>> expression)
        {
            Criteria = expression;
        }
        protected virtual void AddInclude(Expression<Func<T, object>> includeExpression)
        {
            Includes.Add(includeExpression);
        }
        protected virtual void AddInclude(string includeString)
        {
            IncludeStrings.Add(includeString);
        }
    }
}
