﻿using DDDShell.Domain.AggregateModels;
using DDDShell.ViewModels.Options;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDDShell.Infrastructure.Services
{
    public class MailKitEmailService : IEmailService
    {
        private readonly EmailOptions _emailOptions;


        public MailKitEmailService(IOptions<EmailOptions> emailConfig)
        {
            _emailOptions = emailConfig.Value;
        }


        public async Task SendEmailAsync(string to, string subject, string body, IEnumerable<string> attachments = null, IEnumerable<string> attachmentNames = null, string from = null, string fromName = null, string cc = null, string bcc = null)
        {
            if (_emailOptions.Debug)
            {
                //Debug
                string debugMailTo = "";
                string debugMailCc = "";
                string debugMailBcc = "";
                if (!string.IsNullOrWhiteSpace(_emailOptions.DebugAllowedAddresses))
                {
                    string[] allowedEmails = _emailOptions.DebugAllowedAddresses.Split(';');
                    if (!string.IsNullOrEmpty(to))
                    {
                        foreach (string emlTo in to.Split(';'))
                        {
                            if (!string.IsNullOrWhiteSpace(emlTo) && IsDebugAllowedEmail(allowedEmails, emlTo))
                            {
                                debugMailTo += ";" + emlTo;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(cc))
                    {
                        foreach (string emlCC in cc.Split(';'))
                        {
                            if (!string.IsNullOrWhiteSpace(emlCC) && IsDebugAllowedEmail(allowedEmails, emlCC))
                            {
                                debugMailCc += ";" + emlCC;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(bcc))
                    {
                        foreach (string emlBcc in bcc.Split(';'))
                        {
                            if (!string.IsNullOrWhiteSpace(emlBcc) && IsDebugAllowedEmail(allowedEmails, emlBcc))
                            {
                                debugMailBcc += ";" + emlBcc;
                            }
                        }
                    }
                }
                if (
                    (string.IsNullOrWhiteSpace(debugMailTo) && string.IsNullOrWhiteSpace(debugMailCc) && string.IsNullOrWhiteSpace(debugMailBcc)) &&
                        (
                            (!string.IsNullOrWhiteSpace(to) && !string.IsNullOrWhiteSpace(to.Replace(";", ""))) ||
                            (!string.IsNullOrWhiteSpace(cc) && !string.IsNullOrWhiteSpace(cc.Replace(";", ""))) ||
                            (!string.IsNullOrWhiteSpace(bcc) && !string.IsNullOrWhiteSpace(bcc.Replace(";", "")))
                        )
                    ) //Se non c'è nessuna mail con indirizzi di debug, la manda alla mail di debug di default, ma solo se c'è almeno un indirizzo di destinazione (se non c'è nessun indirizzo, sarà la mailkit che blocca l'invio)
                { debugMailTo = _emailOptions.DebugDefaultTo; }
                subject = "DEBUG - " + subject;
                body = "DEBUG - Real To: " + to + " - Real Cc: " + cc + " - Real Bcc: " + bcc + "<br/><br/>" + body;
                to = debugMailTo;
                cc = debugMailCc;
                bcc = debugMailBcc;
            }

            await SendMailkit(
                from: from ?? _emailOptions.FromAddress, fromName: !string.IsNullOrEmpty(from) ? fromName : _emailOptions.FromName,
                to: to, cc: cc,
                bcc: bcc + (!string.IsNullOrEmpty(_emailOptions.DefaultBcc) ? ";" + _emailOptions.DefaultBcc : ""),
                subject: subject, body: body,
                attachments: attachments, attachmentNames: attachmentNames,
                mailServerAddress: _emailOptions.MailServerAddress, mailServerPort: _emailOptions.MailServerPort, userId: _emailOptions.UserId, userPassword: _emailOptions.UserPassword, useSSL: _emailOptions.UseSSL, useTLS: _emailOptions.UseTLS); //Per ora i param del server sono sempre passati dalla config e non da param. In caso di invii con altri server dovrà essere specificata un'altra config (eventualmente dividere tra config a livello di app e config per i dati server)
        }

        private async Task SendMailkit(string from, string to, string subject, string body, string mailServerAddress, int mailServerPort, string userId, string userPassword, IEnumerable<string> attachments = null, IEnumerable<string> attachmentNames = null, string fromName = null, string cc = null, string bcc = null, bool useSSL = false, bool useTLS = false)
        {
            MimeKit.MimeMessage oMsg = new MimeKit.MimeMessage();
            try
            {
                //Imposta il mittente
                oMsg.From.Add(new MimeKit.MailboxAddress(fromName, from));
                if (!string.IsNullOrWhiteSpace(bcc) && bcc.IndexOf(";") >= 0)
                {
                    foreach (string eml in bcc.Split(';'))
                    {
                        if (!string.IsNullOrEmpty(eml.Replace(" ", "")))
                        {
                            oMsg.Bcc.Add(new MimeKit.MailboxAddress(eml.Replace(" ", ""), eml.Replace(" ", "")));
                        }
                    }
                }
                else if (!string.IsNullOrWhiteSpace(bcc))
                {
                    oMsg.Bcc.Add(new MimeKit.MailboxAddress(bcc, bcc));
                }

                if (!string.IsNullOrWhiteSpace(cc) && cc.IndexOf(";") >= 0)
                {
                    foreach (string eml in cc.Split(';'))
                    {
                        if (!string.IsNullOrEmpty(eml.Replace(" ", "")))
                        {
                            oMsg.Cc.Add(new MimeKit.MailboxAddress(eml.Replace(" ", ""), eml.Replace(" ", "")));
                        }
                    }
                }
                else if (!string.IsNullOrWhiteSpace(cc))
                {
                    oMsg.Cc.Add(new MimeKit.MailboxAddress(cc, cc));
                }

                //La proprietà .To è una collezione di destinatari, 
                //quindi possiamo addizionare quanti destinatari vogliamo.
                if (!string.IsNullOrWhiteSpace(to) && to.IndexOf(";") >= 0)
                {
                    foreach (string eml in to.Split(';'))
                    {
                        if (!string.IsNullOrWhiteSpace(eml))
                        {
                            oMsg.To.Add(new MimeKit.MailboxAddress(eml.Replace(" ", ""), eml.Replace(" ", "")));
                        }
                    }
                }
                else if (!string.IsNullOrWhiteSpace(to))
                {
                    oMsg.To.Add(new MimeKit.MailboxAddress(to, to));
                }

                //Se non c'è nessun destinatario ritorno true
                if (oMsg.To.Count == 0 && oMsg.Cc.Count == 0 && oMsg.Bcc.Count == 0)
                    throw new Exception("No recipients");

                //Imposto oggetto
                oMsg.Subject = subject;
                //Imposto contenuto
                var builder = new MimeKit.BodyBuilder();
                builder.HtmlBody = body;

                //imposto gli allegati
                if ((attachments != null))
                {
                    if (attachments.Count() > 0)
                    {
                        foreach (string all in attachments)
                        {
                            if (!string.IsNullOrWhiteSpace(all))
                            {
                                builder.Attachments.Add(all);
                            }
                        }
                    }
                }

                //rename allegati
                for (int attIndex = 0; attIndex < builder.Attachments.Count; attIndex++)
                {
                    if (builder.Attachments[attIndex].IsAttachment && (attachmentNames != null && attachmentNames.Count() > attIndex))
                    {
                        builder.Attachments[attIndex].ContentDisposition.FileName = attachmentNames.ElementAt(attIndex);
                    }
                }

                oMsg.Body = builder.ToMessageBody();

                //Imposto il Server Smtp
                //Metto valori di default da web.config nel caso si passi SmtpClient Nothing
                MailKit.Net.Smtp.SmtpClient client;
                try
                {
                    client = new MailKit.Net.Smtp.SmtpClient();
                    await client.ConnectAsync(mailServerAddress, mailServerPort, useTLS ? MailKit.Security.SecureSocketOptions.StartTls : (useSSL ? MailKit.Security.SecureSocketOptions.SslOnConnect : MailKit.Security.SecureSocketOptions.None)).ConfigureAwait(false);
                }
                catch (MailKit.Security.AuthenticationException ex)
                {
                    if (ex.InnerException is System.ComponentModel.Win32Exception)
                    {
                        //Fix per gestione bug .net framework / mailkit
                        //https://github.com/dotnet/corefx/issues/1854#issuecomment-160513245
                        client = new MailKit.Net.Smtp.SmtpClient();
                        await client.ConnectAsync(mailServerAddress, mailServerPort, useTLS ? MailKit.Security.SecureSocketOptions.StartTls : (useSSL ? MailKit.Security.SecureSocketOptions.SslOnConnect : MailKit.Security.SecureSocketOptions.None)).ConfigureAwait(false);
                    }
                    else
                    {
                        throw;
                    }
                }

                using (client)
                {
                    try
                    {
                        // Note: since we don't have an OAuth2 token, disable
                        // the XOAUTH2 authentication mechanism.
                        client.AuthenticationMechanisms.Remove("XOAUTH2");

                        // Note: only needed if the SMTP server requires authentication
                        await client.AuthenticateAsync(userId, userPassword);
                        await client.SendAsync(oMsg).ConfigureAwait(false);
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        await client.DisconnectAsync(true).ConfigureAwait(false);
                    }
                }
            }
            catch // (Exception ex)
            {
                //System.Web.HttpContext.Current.Response.Write(Ex.ToString());
                //errorMessage = ex.ToString();
                throw;
            }
            finally
            {
                oMsg = null;
            }
        }

        private bool IsDebugAllowedEmail(string[] allowedEmails, string email)
        {
            foreach (string allowedEmail in allowedEmails)
            {
                if (allowedEmail.IndexOf("@") == 0)
                {
                    //Controllo dominio
                    if (email.EndsWith(allowedEmail, StringComparison.OrdinalIgnoreCase))
                        return true;
                }
                else
                {
                    //Controllo casella esatta
                    if (email.Equals(allowedEmail, StringComparison.OrdinalIgnoreCase))
                        return true;
                }
            }
            return false;
        }
    }
}