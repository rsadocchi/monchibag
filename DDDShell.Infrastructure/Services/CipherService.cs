﻿using DDDShell.Domain.AggregateModels;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;

namespace DDDShell.Infrastructure.Services
{
    public class CipherService : ICipherService
    {
        private readonly IConfiguration _configuration;
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private string Key = "";

        public CipherService(IDataProtectionProvider dataProtectionProvider, IConfiguration configuration)
        {
            _dataProtectionProvider = dataProtectionProvider;
            _configuration = configuration;

            Key = _configuration.GetValue<string>("CipherOptions:CipherServiceKey");
        }

        public string Encrypt(string input, bool urlEncode = false)
        {
            var protector = _dataProtectionProvider.CreateProtector(Key);
            var encryptedString = protector.Protect(input);
            if (!urlEncode)
                return encryptedString;
            else
                return System.Net.WebUtility.UrlEncode(encryptedString);
        }

        public string Decrypt(string cipherText, bool urlEncode = false)
        {
            var protector = _dataProtectionProvider.CreateProtector(Key);
            if (urlEncode)
                cipherText = System.Net.WebUtility.UrlDecode(cipherText);
            return protector.Unprotect(cipherText);
        }
    }
}
