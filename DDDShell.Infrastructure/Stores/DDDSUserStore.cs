﻿using DDDShell.Context;
using DDDShell.Domain.AggregateModels;
using DDDShell.Security.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DDDShell.Infrastructure
{
    public class DDDSUserStore : Microsoft.AspNetCore.Identity.EntityFrameworkCore.UserStore<User, Role, DDDShellDbContext, int, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>, IDDDSUserStore<User>
    {
        public DDDSUserStore(
            DDDShellDbContext context,
            IdentityErrorDescriber describer = null)
            : base(context, describer)
        { }

        //protected override UserClaim CreateUserClaim(User user, Claim claim)
        //{
        //    var userClaim = new UserClaim { UserId = user.Id };
        //    userClaim.InitializeFromClaim(claim);
        //    return userClaim;
        //}

        //protected override UserLogin CreateUserLogin(User user, UserLoginInfo login)
        //{
        //    return new UserLogin
        //    {
        //        UserId = user.Id,
        //        ProviderKey = login.ProviderKey,
        //        LoginProvider = login.LoginProvider,
        //        ProviderDisplayName = login.ProviderDisplayName
        //    };
        //}

        //protected override UserRole CreateUserRole(User user, Role role)
        //{
        //    return new UserRole()
        //    {
        //        UserId = user.Id,
        //        RoleId = role.Id
        //    };
        //}

        //protected override UserToken CreateUserToken(User user, string loginProvider, string name, string value)
        //{
        //    return new UserToken
        //    {
        //        UserId = user.Id,
        //        LoginProvider = loginProvider,
        //        Name = name,
        //        Value = value
        //    };
        //}

        /// <summary>
        /// Retrieves all users with the specified claim.
        /// </summary>
        /// <param name="claim">The claim whose users should be retrieved.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>
        /// The <see cref="Task"/> contains a list of users, if any, that contain the specified claim. 
        /// </returns>
        public async Task<IList<User>> GetUsersForClaimsIncludeRoleClaimsAsync(Claim[] claims, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            if (claims == null)
            {
                throw new ArgumentNullException(nameof(claims));
            }

            var _userClaims = Context.Set<UserClaim>();
            var _userRoles = Context.Set<UserRole>();
            var _roles = Context.Set<Role>();
            var _roleClaims = Context.Set<RoleClaim>();

            //Claim negli UserClaim o nei RoleClaim
            IQueryable<User> query = Users;
            foreach (var claim in claims)
                query = query.Where(u => _userClaims.Where(uc => uc.ClaimValue == claim.Value
                       && uc.ClaimType == claim.Type).Select(uc => uc.UserId).Contains(u.Id) ||
                       _roles.Where(r => _roleClaims.Where(rc => rc.ClaimValue == claim.Value
                       && rc.ClaimType == claim.Type).Select(rc => rc.RoleId).Contains(r.Id) &&
                       _userRoles.Where(ur => ur.UserId == u.Id).Select(ur => ur.RoleId).Contains(r.Id)
                       ).Any()
           );

            return await query.ToListAsync(cancellationToken);
        }

        /// <summary>
        /// Retrieves all users with the specified claim.
        /// </summary>
        /// <param name="claims">The claim whose users should be retrieved.</param>
        /// <param name="cancellationToken">The <see cref="CancellationToken"/> used to propagate notifications that the operation should be canceled.</param>
        /// <returns>
        /// The <see cref="Task"/> contains a list of users, if any, that contain the specified claim. 
        /// </returns>
        public async Task<IList<Role>> GetRolesForClaimsAsync(Claim[] claims, CancellationToken cancellationToken = default(CancellationToken))
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            if (claims == null)
            {
                throw new ArgumentNullException(nameof(claims));
            }

            var _roles = Context.Set<Role>();
            var _roleClaims = Context.Set<RoleClaim>();

            //Claim negli UserClaim o nei RoleClaim
            IQueryable<Role> query = _roles;
            foreach (var claim in claims)
                query = query.Where(r => _roleClaims.Where(rc => rc.ClaimType == claim.Type && rc.ClaimValue == claim.Value && rc.RoleId == r.Id).Any());
            return await query.ToListAsync(cancellationToken);
        }
    }
}