﻿using DDDShell.Context;
using DDDShell.Domain.AggregateModels;
using DDDShell.Security.Identity;
using Microsoft.AspNetCore.Identity;

namespace DDDShell.Infrastructure
{
    public class DDDSRoleStore : Microsoft.AspNetCore.Identity.EntityFrameworkCore.RoleStore<Role, DDDShellDbContext, int, UserRole, RoleClaim>, IDDDSRoleStore<Role>
    {
        public DDDSRoleStore(
            DDDShellDbContext context,
            IdentityErrorDescriber describer = null)
            : base(context, describer)
        { }
    }
}